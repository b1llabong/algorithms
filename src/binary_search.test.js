import { binarySearch } from './binary_search.ts'

const array = [1 ,2 ,3 ,4 ,5 ,6, 7, 8, 9, 10, 11, 12, 14, 15]
const target = 3
const target2 = 8
const target3 = 15
const target4 = 20
const target5 = 0

describe('Binary search should return', () => {
    
    test('index of searching item', () => {
        expect(binarySearch(array, target)).toBe(2)
    })

    test('index of searching item', () => {
        expect(binarySearch(array, target2)).toBe(7)
    })

    test('index of searching item', () => {
        expect(binarySearch(array, target3)).toBe(13)
    })

    test('null', () => {
        expect(binarySearch(array, target4)).toBe(null)
    })

    test('null', () => {
        expect(binarySearch(array, target5)).toBe(null)
    })

})
