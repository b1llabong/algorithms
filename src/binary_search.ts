/*
    Function search target value in sorted ascending array iteratively.
    If target value does not exist, function return null.
*/
export const binarySearch = (arr: any[], target: number) => {

    let left: number = arr[0]
    let right: number = arr.length

    while (!(left >= right)) {

        let mid: number = Math.floor(left + (right - left) / 2)

        if (arr[mid] == target) {
            return mid
        }
        if (arr[mid] > target) {
            right = mid
        } else {
            left = mid + 1
        }
    }
    return null
}